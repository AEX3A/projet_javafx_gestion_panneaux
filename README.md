# Projet_JavaFX_Gestion_Panneaux
This is a school project to learn Java and JavaFX.

# Projet JavaFX Gestion Panneaux
### Informations
- description : my take on the classic Atari game "Asteroids"
- type : school project
- date : 2018
---
### Usage
- prerequisites :
  - The language Java and its virtual machine JVM (download Java jdk 11 [here](https://www.oracle.com/java/technologies/downloads/#java11))
  - The librairy JavaFX (download JavaFX 17 [here](https://gluonhq.com/products/javafx/))
- execution :
  - open the project folder in the file explorator
  - execute the program via ```py start.py <javaFX_path>```
    - javaFX_path : absolute path to the lib folder of the javaFX folder