import os
import sys

def usages():
    print("Usage: start.py \"<javaFX_path>\"")
    print("javaFX_path : absolute path to the lib folder of the javaFX folder")

if len(sys.argv) < 2:
    print("Error: the second parameter is missing...")
    usages()
    exit()
if not os.path.isdir(sys.argv[1]):
    print("Error: the second parameter is not a valid path...")
    usages()
    exit()

javaFX_path = sys.argv[1]
os.system("java --module-path \"" + javaFX_path + "\" --add-modules javafx.controls,javafx.fxml,javafx.graphics,javafx.web -jar \"Projet/out/artifacts/Projet_jar/Projet.jar\"")