import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreMessagePanneau extends Stage {
	// les widgets de la fenêtre
		private VBox 			racine				= new VBox();
		private GridPane 		zoneSaisie 			= new GridPane();
		private HBox 			zoneBoutons 		= new HBox(20);
		private Label 			lblMessage			= new Label("Message � afficher :");
		private TextField		txtMessage			= new TextField();
		private Button 			bnOK				= new Button("OK");
		private Button 			bnAnnuler			= new Button("Annuler");
		private int indexPanneau;
				
		// constructeur : initialisation de la fenêtre
		public FenetreMessagePanneau(int i){
			this.indexPanneau=i;
			this.setTitle("Modifier le message du panneau");
			//this.setResizable(false);
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		// création du Scene graph
		private Parent creerContenu() {
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);
			this.bnOK.setPrefWidth(100);
			this.bnAnnuler.setPrefWidth(100);
			
			this.bnAnnuler.setOnAction(e-> this.fermer());
			this.bnOK.setDisable(true);
			this.txtMessage.setOnKeyReleased(e -> griserBoutons());
			this.bnOK.setOnAction(e->ajoutok());

			
			zoneSaisie.setHgap(10);
			zoneSaisie.setVgap(20);
			
			zoneSaisie.add(lblMessage,  0,  0);
			zoneSaisie.add(txtMessage,  1,  0);
						
			zoneBoutons.getChildren().addAll(bnOK, bnAnnuler);
			zoneBoutons.setAlignment(Pos.CENTER_RIGHT);
			
			racine.getChildren().addAll(zoneSaisie, zoneBoutons);
			VBox.setMargin(zoneSaisie, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

			// détection des événements
			// A COMPLETER
			
			return racine;
		}

		
		public void griserBoutons(){
			if(txtMessage.getText().isEmpty())
			{
				bnOK.setDisable(true);
			}
			else
			{
				bnOK.setDisable(false);
			}
		}
		
		public void initMessage()
		{
			this.txtMessage.setText(PrincipaleFX.getPanneauLP(this.indexPanneau).getMessage());			
		}
		
		public void ajoutok(){
			try {
				PrincipaleFX.modifierMessagePanneau(indexPanneau, this.txtMessage.getText());
			}
			catch (java.lang.NumberFormatException e){
				System.out.println("format incorrect !");
			}
			
			this.close();
		}

		
		public void fermer()
		{
			this.close();
		}

}
