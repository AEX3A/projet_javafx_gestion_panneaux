import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;


public class Panneau implements Serializable {
	private String id;
	private double x;
	private double y;
	private String message;
	private Client client;
	
	Panneau(String i, double xx, double yy) 
	{
		this.id=i;
		this.x=xx;
		this.y=yy;
		this.message=null;
		this.client=null;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public String getId() {
		return id;
	}
	
	public String getMessage() {
		return message;
	}

	public Client getClient() {
		return client;
	}

	public String toString()
	{	
		return "Panneau "+this.id+" : \nx : "+this.x+"\ny : "+this.y;
	}
	
	public void afficher()
	{
		System.out.println(this.toString());
	}
	
	public void deplacerPanneau(int x, int y)
	{
		this.setX(x);
		this.setY(y);
	}
	
	/* PARTIE MESSAGE */

	public void ajouterMessage(String message) {
		this.message = message;
	}
	
	public void supprMessage()
	{
		this.message=null;
	}
	
	/* PARTIE CLIENT */
	
	public void ModifierClient(Client c)
	{
		if(c!=null && !c.equals(client))
			{	
				this.client=c;
			}
			else
			{
				System.out.println("ajout panneau impossible !");
			}
		}

	public void supprClient(Client c)
	{
		if(c!=null && c.equals(client))
		{
			this.client=null;
		}
		else
		{
			System.out.println("suppression panneau impossible !");
		}
	}
	   
}

