import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Client implements Serializable{
	private String nom;
	private String domaine;
	private HashMap<Panneau,Facture> tabPF;
	
	Client(String n, String d)
	{
		this.nom=n;
		this.domaine=d;
		this.tabPF = new HashMap<Panneau,Facture>(50);
	}

	public String getNom() {
		return nom;
	}

	public String getDomaine() {
		return domaine;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDomaine(String domaine) {
		this.domaine = domaine;
	}

	public String toString()
	{
		return "nom du client : "+this.nom+"\ndomaine du client : "+this.domaine;
	}
	
	public void afficher()
	{
		System.out.println(this.toString());
		if(this.tabPF.size()!=0)
		{
			this.listerPanneaux();
			this.listerFactures();
		}

	}
	
/* PARTIE PANNEAUX/FACTURES */
	
	void ajouterPanneau(Panneau p, Facture f)
	{
		if(p!=null && f!=null && !this.tabPF.containsKey(p))
		{
			this.tabPF.put(p,f);
		}
		else
		{
			System.out.println("ajout panneau (du client) impossible !");
		}
	}
	
	void supprPanneau(Panneau p, Facture f)
	{
		if(p!=null && f!=null && this.tabPF.containsKey(p) && this.tabPF.containsValue(f))
		{
			this.tabPF.remove(p,f);
		}
		else
		{
			System.out.println("suppression panneau (du client) impossible !");
		}
	}
	
	Panneau getPanneau(int i)
	{
		Set<Panneau> setPanneaux=this.tabPF.keySet();
		Panneau[] tabP = setPanneaux.toArray(new Panneau[setPanneaux.size()]);
		return tabP[i];
	}
	
	
	
	Facture getFacture(int i)
	{
		Collection<Facture> collectionf=this.tabPF.values();
		Facture[] tabF = collectionf.toArray((new Facture[collectionf.size()]));
		return tabF[i];
	}
	
	boolean containsPanneau(Panneau p)
	{
		if(this.tabPF.containsKey(p))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	void listerPanneaux()
	{
	    List<Panneau> tabP = new ArrayList<>(this.tabPF.keySet());
	    for(int i=0;i<tabP.size();i++)
	    {
	    	System.out.println(tabP.get(i));
	    }
	}
	
	void listerFactures()
	{
		List<Facture> tabF = new ArrayList<>(this.tabPF.values());
	    for(int i=0;i<tabF.size();i++)
	    {
	    	System.out.println(tabF.get(i));
	    }
	}
	
	int taillePF()
	{
		return this.tabPF.size();
	}
}