import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class FenetreListePanneaux extends Stage {
	// les composants de la fenêtre
		
	private AnchorPane  		racine			= new AnchorPane();
	public ListView<String> 	listePanneaux	= new ListView<String>();
	private Button 				bnAjouter 		= new Button("Ajouter...");
	private Button 				bnSupprimer 	= new Button("Supprimer");
	private Button 				bnFermer 		= new Button("Fermer");
	private Button 				bnDeplacer 		= new Button("Deplacer...");
	private Button 				bnMessage 		= new Button("Modifier Message...");
	private Button 				bnAfficher 		= new Button("Afficher...");



	// constructeur : initialisation de la fenêtre
	public FenetreListePanneaux(){
		this.setTitle("Liste des Panneaux");
		this.setMinHeight(350);
		this.setMinWidth(350);

		this.setResizable(true);
		this.setScene(new Scene(creerContenu(),350,350));	
	}
	
	// creation du Scene graph
	private Parent creerContenu() {
		bnAjouter.setPrefWidth(130);
		bnAfficher.setPrefWidth(130);
		bnDeplacer.setPrefWidth(130);
		bnMessage.setPrefWidth(130);
		bnSupprimer.setPrefWidth(130);
		bnAfficher.setDisable(true);	
		bnDeplacer.setDisable(true);	
		bnMessage.setDisable(true);		
		bnSupprimer.setDisable(true);		
		bnFermer.setPrefWidth(130);
		bnFermer.setPrefHeight(50);
		
		listePanneaux.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		listePanneaux.setOnMouseClicked(e->{griserBoutons();});
		this.listePanneaux.setOnMouseClicked(e -> {griserBoutons();
		doubleClick(e);});
		
		AnchorPane.setTopAnchor(bnAjouter, 10.0);
		AnchorPane.setRightAnchor(bnAjouter, 10.0);
		bnAjouter.setOnAction(e->PrincipaleFX.ouvrirFenetreAjoutPanneau());
		
		AnchorPane.setTopAnchor(bnAfficher, 80.0);
		AnchorPane.setRightAnchor(bnAfficher, 10.0);
		bnAfficher.setOnAction(e->PrincipaleFX.ouvrirFenetreAjoutPanneau());
		bnAfficher.setOnAction(e ->PrincipaleFX.ouvrirFenetreInfosPanneau(this.listePanneaux.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnDeplacer, 120.0);
		AnchorPane.setRightAnchor(bnDeplacer, 10.0);
		bnDeplacer.setOnAction(e->griserBoutons());
		bnDeplacer.setOnAction(e->PrincipaleFX.ouvrirFenetreDeplacerPanneau(this.listePanneaux.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnMessage, 160.0);
		AnchorPane.setRightAnchor(bnMessage, 10.0);
		bnMessage.setOnAction(e->griserBoutons());
		bnMessage.setOnAction(e -> PrincipaleFX.ouvrirFenetreMessagePanneau(this.listePanneaux.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnSupprimer, 200.0);
		AnchorPane.setRightAnchor(bnSupprimer, 10.0);
		bnSupprimer.setOnAction(e->griserBoutons());
		bnSupprimer.setOnAction(e ->suppression());
		
		AnchorPane.setBottomAnchor(bnFermer, 10.0);
		AnchorPane.setRightAnchor(bnFermer, 10.0);
		//bnModifier.setOnAction(e->{Principale.ouvrirFenetreModifier(listeEtudiants.setOnKeyReleased(), listeEtudiants.getSelectionModel().getSelectedIndex());
		bnFermer.setOnAction(e->this.fermer());
		
		AnchorPane.setTopAnchor(listePanneaux, 10.0);
		AnchorPane.setLeftAnchor(listePanneaux, 10.0);
		AnchorPane.setRightAnchor(listePanneaux, 150.0);
		AnchorPane.setBottomAnchor(listePanneaux, 10.0);
		

		racine.getChildren().addAll(bnAjouter, bnAfficher, bnDeplacer, bnMessage, bnSupprimer, bnFermer, listePanneaux);
					
		// détection et traitement des événements
		// A FAIRE : poser des écouteurs sur les composants de la fenêtre
		return racine;
	}
	
	public void actualiserListePanneaux() {
		for(int i=0;i<PrincipaleFX.tailleLP();i++)
		{
			listePanneaux.getItems().add(PrincipaleFX.getPanneauLP(i).getId());
		}	
	}
	
	public void clearListePanneaux()
	{
		listePanneaux.getItems().clear();
	}
	
	public void initBoutons()
	{
		this.bnAfficher.setDisable(true);
		this.bnDeplacer.setDisable(true);
		this.bnMessage.setDisable(true);
		this.bnSupprimer.setDisable(true);
	}
	
	public void griserBoutons(){
		if(listePanneaux.getSelectionModel().getSelectedIndex()!=-1 && listePanneaux.getItems().size()>0)
		{
			bnSupprimer.setDisable(false);
			bnDeplacer.setDisable(false);
			bnMessage.setDisable(false);
			bnAfficher.setDisable(false);
		}
		else 
		{
			bnSupprimer.setDisable(true);
			bnDeplacer.setDisable(true);
			bnMessage.setDisable(true);
			bnAfficher.setDisable(true);
		}
	}
	
	public void doubleClick(MouseEvent e)
	{
		if (e.getClickCount()==2){
			PrincipaleFX.ouvrirFenetreInfosPanneau(this.listePanneaux.getSelectionModel().getSelectedIndex());
		}
	}
	
	public void suppression(){
		Alert alert=new Alert(AlertType.CONFIRMATION,"Voulez-vous vraiment le supprimer",ButtonType.NO,ButtonType.YES);
		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get()==ButtonType.YES){
			
			int trouve=-1;
			int i=0;
			while(trouve==-1 && i<PrincipaleFX.tailleLP())
			{
				if((PrincipaleFX.getPanneauLP(i).getId()).equals(this.listePanneaux.getSelectionModel().getSelectedItem()))
				{
					trouve=i;
				}
				i++;
			}
			if(trouve!=-1)
			{
				PrincipaleFX.supprimerPanneau(PrincipaleFX.getPanneauLP(trouve));
			}
			else
			{
				System.out.println("erreur de suppression");
			}
			
			//PrincipaleFX.supprimerPanneau(this.listePanneaux.getSelectionModel().getSelectedIndex());
		}
	}
	
	public void fermer()
	{
		this.close();
		
	}
}