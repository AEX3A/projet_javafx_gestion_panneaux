import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreInfosPanneau extends Stage {
	// les composants de la fenêtre
		
	private VBox 				root		 	= new VBox();
	private Label				lblPanneau		= new Label();
	private Label				lblMessage		= new Label();
	private Label				lblClient		= new Label();
	private Button 				bnFermer 		= new Button("Fermer");


	// constructeur : initialisation de la fenêtre
	public FenetreInfosPanneau(int i){
		if(PrincipaleFX.getPanneauLP(i)!=null)
		{
			this.lblPanneau.setText(PrincipaleFX.getPanneauLP(i).toString());			
		}
		if(PrincipaleFX.getPanneauLP(i).getMessage()!=null)
		{
			this.lblMessage.setText("Message : \n"+PrincipaleFX.getPanneauLP(i).getMessage());
		}
		if(PrincipaleFX.getPanneauLP(i).getClient()!=null)
		{
			this.lblClient.setText(PrincipaleFX.getPanneauLP(i).getClient().toString());
		}		this.setTitle("Affichage du Panneau");
		this.setMinWidth(200);
		this.setScene(new Scene(creerContenu()));	
		this.sizeToScene();
	}
	
	public FenetreInfosPanneau(Panneau p){
		if(p!=null)
		{
			this.lblPanneau.setText(p.toString());			
		}
		if(p.getMessage()!=null)
		{
			this.lblMessage.setText("Message : \n"+p.getMessage());
		}
		if(p.getClient()!=null)
		{
			this.lblClient.setText(p.getClient().toString());
		}		this.setTitle("Affichage du Panneau");
		this.setMinWidth(200);
		this.setScene(new Scene(creerContenu()));	
		this.sizeToScene();
	}
	
	// création du Scene graph
	private Parent creerContenu() {
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		bnFermer.setPrefWidth(130);
		bnFermer.setOnAction(e -> fermer());

		root.getChildren().addAll(lblPanneau,lblMessage,lblClient, bnFermer);
		root.setMargin(bnFermer, new Insets(10));
		root.setMargin(lblPanneau, new Insets(20));
		root.setMargin(lblMessage, new Insets(20));
		root.setMargin(lblClient, new Insets(20));
		root.setAlignment(Pos.CENTER);
		
		return root;
	}
	
	
	public void fermer()
	{
		this.close();
		
	}
}