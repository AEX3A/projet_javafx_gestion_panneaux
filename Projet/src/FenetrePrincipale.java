import java.awt.Paint;
import java.util.Optional;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class FenetrePrincipale extends Stage {
	// les composants de la fenêtre
	private BorderPane			racine			= new BorderPane();
	private AnchorPane  		zoneBoutons		= new AnchorPane();
	private AnchorPane  		zoneCarte		= new AnchorPane();
	private Image 				imagecarte 		= new Image("mapLannion.PNG");
	private Image 				imagePanneau 	= new Image("panneau.png");
	private Label				lblutilisation	= new Label("Utilisation du logiciel : \n"
															+ "Creation d'un panneau : \n"
															+ "- double click sur la carte \n"
															+ "- ajout � partir de la liste des panneaux \n\n"
															+ "Deplacement d'un panneau : \n"
															+ "- glisser d�poser sur la carte \n"
															+ "- deplacement � partir de la liste des panneaux \n\n"
															+ "Modification du message d'un panneau : \n"
															+ "- modification � partir de la liste des panneaux \n\n"
															+ "Affichage d'un panneau : \n"
															+ "- simple click droit sur un panneau de la carte \n"
															+ "- affichage � partir de la liste des panneaux \n\n"
															+ "Gestion des clients : \n"
															+ "- gestion des clients � partir de la liste des clients");
	
	private Button				bnAjouter		= new Button("Ajouter Panneau");
	private Button				bnLPanneaux		= new Button("Liste Panneaux...");
	private Button				bnLClients		= new Button("Liste Clients...");
	private Button 				bnFermer 		= new Button("Fermer");
	private MenuItem 			optionAjouter 	= new MenuItem("Ajouter");
	private MenuItem 			optionAfficher	= new MenuItem("Afficher");
	private MenuItem 			optionSupprimer = new MenuItem("Supprimer");
	final int 					numCols 		= 100 ;
    final int 					numRows 		= 100 ;
    double orgSceneX, orgSceneY;
	double orgTranslateX, orgTranslateY; 
	int indexPanneauBeforeDrag=-1;
	double newX,newY;
    
	private ContextMenu menu = new ContextMenu( optionAjouter,new SeparatorMenuItem(),optionAfficher,new SeparatorMenuItem(),optionSupprimer);


	// constructeur : initialisation de la fenêtre
	public FenetrePrincipale(){
		this.setTitle("Fenetre Principale");
		this.setResizable(false);
		this.setScene(new Scene(creerContenu()));	
		this.sizeToScene();
	}
	
	// création du Scene graph
	private Parent creerContenu() {
		bnAjouter.setPrefWidth(150);
		bnAjouter.setPrefHeight(50);
		bnLPanneaux.setPrefWidth(150);
		bnLPanneaux.setPrefHeight(50);
		bnLClients.setPrefWidth(150);
		bnLClients.setPrefHeight(50);
		bnFermer.setPrefWidth(150);
		bnFermer.setPrefHeight(50);
				
		AnchorPane.setTopAnchor(lblutilisation, 10.0);
		AnchorPane.setRightAnchor(lblutilisation, 10.0);
		AnchorPane.setLeftAnchor(lblutilisation, 10.0);
		
		AnchorPane.setBottomAnchor(bnLClients, 130.0);
		AnchorPane.setRightAnchor(bnLClients, 10.0);
		AnchorPane.setLeftAnchor(bnLClients, 10.0);
		bnLClients.setOnAction(e -> PrincipaleFX.ouvrirFenetreListeClient());
		AnchorPane.setBottomAnchor(bnLPanneaux, 70.0);
		AnchorPane.setRightAnchor(bnLPanneaux, 10.0);
		AnchorPane.setLeftAnchor(bnLPanneaux, 10.0);
		bnLPanneaux.setOnAction(e -> PrincipaleFX.ouvrirFenetreListePanneaux());
		
		AnchorPane.setBottomAnchor(bnFermer, 10.0);
		AnchorPane.setRightAnchor(bnFermer, 10.0);
		AnchorPane.setLeftAnchor(bnFermer, 10.0);
		bnFermer.setOnAction(e->this.fermer());
		zoneBoutons.getChildren().addAll(lblutilisation, bnLPanneaux, bnLClients, bnFermer);
      
		this.clearCarte();
		
		racine.setLeft(zoneCarte);
		racine.setRight(zoneBoutons);
		

		return racine;
	}
	
	public void ajoutPanneauCarte(double x, double y)
	{
		ImageView panneau = new ImageView();
		panneau.setFitWidth(30);
		panneau.setFitHeight(30);
		panneau.setImage(imagePanneau);
		AnchorPane.setLeftAnchor(panneau, x-13.0);
		AnchorPane.setTopAnchor(panneau, y-24.0);
		zoneCarte.getChildren().add(panneau);
		panneau.setCursor(Cursor.HAND);
		panneau.setOnMouseClicked(e -> infosPanneau(e));
		panneau.setOnMousePressed(circleOnMousePressedEventHandler);
		panneau.setOnMouseDragged(circleOnMouseDraggedEventHandler);
		panneau.setOnMouseReleased(e -> this.deplacerPanneauCarte(e));
	}
	
	public void clearCarte()
	{
		ImageView carte = new ImageView();
		carte.setFitWidth(1000);
		carte.setFitHeight(600);
		carte.setImage(imagecarte);	
		zoneCarte.getChildren().clear();
		zoneCarte.getChildren().add(carte);
        carte.setOnMouseClicked(e -> ClickCarte(e.getX(),e.getY(),e));
	}
	
	public void ClickCarte(double x, double y, MouseEvent e)
	{
		if (e.getClickCount()==2 && e.getButton().equals(MouseButton.PRIMARY) && e.getX()<=987 && e.getX()>=13 && e.getY()<=595 && e.getY()>=24)
		{
			PrincipaleFX.ouvrirFenetreAjoutPanneau(x, y);
		}
		else if (e.getClickCount()!=2 || e.getButton().equals(MouseButton.SECONDARY) )
		{
			
		}
		else
		{
			Alert alert=new Alert(AlertType.ERROR,"La cr�ation de panneau n'est pas autoris� � cet endroit !",ButtonType.OK);
			Optional<ButtonType> result = alert.showAndWait();
		}
	}
	
	public void infosPanneau(MouseEvent e)
	{
		if(e.getClickCount()==1 && e.getButton().equals(MouseButton.SECONDARY))
		{
			Node source = (Node) e.getSource();
			int trouve=-1;
			int i=0;
			while(trouve==-1 && i<PrincipaleFX.tailleLP())
			{
				if(PrincipaleFX.getPanneauLP(i).getX()==source.getLayoutX()+13.0 && PrincipaleFX.getPanneauLP(i).getY()==source.getLayoutY()+24.0)
				{
					trouve=i;
				}
				i++;
			}
			if(trouve!=-1)
			{
				PrincipaleFX.ouvrirFenetreInfosPanneau(PrincipaleFX.getPanneauLP(trouve));
			}
		}
	}
	
	EventHandler<MouseEvent> circleOnMousePressedEventHandler = 
	        new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent t) {
	        	if(t.getButton().equals(MouseButton.PRIMARY))
	    		{
	        		orgSceneX = t.getSceneX();
		            orgSceneY = t.getSceneY();
		            orgTranslateX = ((ImageView)(t.getSource())).getTranslateX();
		            orgTranslateY = ((ImageView)(t.getSource())).getTranslateY();
		            Node source = (Node) t.getSource();
					int trouve=-1;
					int i=0;
					while(trouve==-1 && i<PrincipaleFX.tailleLP())
					{
						if(PrincipaleFX.getPanneauLP(i).getX()==source.getLayoutX()+13.0 && PrincipaleFX.getPanneauLP(i).getY()==source.getLayoutY()+24.0)
						{
							trouve=i;
						}
						i++;
					}
					if(trouve!=-1)
					{
						indexPanneauBeforeDrag=trouve;
					}
	    		}
	        }
	    };
	     
	    EventHandler<MouseEvent> circleOnMouseDraggedEventHandler = 
	        new EventHandler<MouseEvent>() {
	 
	        @Override
	        public void handle(MouseEvent t) {
	        	if(t.getButton().equals(MouseButton.PRIMARY))
	    		{
	        		double offsetX = t.getSceneX() - orgSceneX;
			        double offsetY = t.getSceneY() - orgSceneY;
			        double newTranslateX = orgTranslateX + offsetX;
			        double newTranslateY = orgTranslateY + offsetY;
			        ((ImageView)(t.getSource())).setTranslateX(newTranslateX);
			        ((ImageView)(t.getSource())).setTranslateY(newTranslateY);
			        newX=((ImageView)(t.getSource())).getLayoutX()+newTranslateX;
			        newY=((ImageView)(t.getSource())).getLayoutY()+newTranslateY;
			        Node source = (Node) t.getSource();
					int trouve=-1;
					int i=0;
					while(trouve==-1 && i<PrincipaleFX.tailleLP())
					{
						if(PrincipaleFX.getPanneauLP(i).getX()==source.getLayoutX()+13.0 && PrincipaleFX.getPanneauLP(i).getY()==source.getLayoutY()+24.0)
						{
							trouve=i;
						}
						i++;
					}
					if(trouve!=-1)
					{
						indexPanneauBeforeDrag=trouve;
					}
					System.out.println(source.getLayoutX());
	    		}
	        }
	    };
	    
	public void deplacerPanneauCarte(MouseEvent t)
	{
		if(t.getClickCount()==1 && t.getButton().equals(MouseButton.PRIMARY))
		{
			if(indexPanneauBeforeDrag!=-1 && newX+13.0>13 && newX+13.0<987 && newY+24.0>24 && newY+24.0<595)
			{
				PrincipaleFX.deplacerPanneau(indexPanneauBeforeDrag, newX+13.0, newY+24.0);

			}
			else
			{
		        Node source = (Node) t.getSource();
				PrincipaleFX.deplacerPanneau(indexPanneauBeforeDrag, source.getLayoutX()+13.0, source.getLayoutY()+24.0);
				Alert alert=new Alert(AlertType.ERROR,"Le panneau ne peut pas �tre d�plac� � cet endroit !",ButtonType.OK);
				Optional<ButtonType> result = alert.showAndWait();
			}
		}
	}
	
	public void fermer()
	{
		Alert alert=new Alert(AlertType.CONFIRMATION,"Voulez-vous sauvegarder les donn�es ? Les donn�es sauvegard�es lors d'une autre session seront supprim�es !",ButtonType.NO,ButtonType.YES);
		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get()==ButtonType.YES){
			PrincipaleFX.sauvegarder();
		}
		System.exit(0);
	}
}