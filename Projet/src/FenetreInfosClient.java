import java.util.Date;
import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreInfosClient extends Stage {
	// les composants de la fenêtre
		
	private BorderPane 			root		 	= new BorderPane();
	private ListView<Panneau>	listePanneaux	= new ListView<Panneau>();
	private ListView<Facture>	listeFactures	= new ListView<Facture>();
	private Label				lblClient		= new Label();
	private Button 				bnFermer 		= new Button("Fermer");
	private int indexClient;
	private Date date;


	// constructeur : initialisation de la fenêtre
	public FenetreInfosClient(int i){
		if(PrincipaleFX.getClientLC(i)!=null)
		{
			this.lblClient.setText(PrincipaleFX.getClientLC(i).toString());			
		}
		indexClient=i;
		this.setTitle("Affichage du Client");
		this.setMinWidth(200);
		this.setScene(new Scene(creerContenu()));	
		this.sizeToScene();
	}
	
	// création du Scene graph
	private Parent creerContenu() {
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		bnFermer.setPrefWidth(130);
		bnFermer.setOnAction(e -> fermer());
				
		root.setTop(lblClient);
		root.setCenter(listePanneaux);
		root.setRight(listeFactures);
		root.setBottom(bnFermer);

		BorderPane.setMargin(lblClient, new Insets(10,10,0,10));
		BorderPane.setMargin(listePanneaux, new Insets(10,10,0,10));
		BorderPane.setMargin(listeFactures, new Insets(10,10,0,10));
		BorderPane.setMargin(bnFermer, new Insets(10));
		BorderPane.setAlignment(bnFermer, Pos.BOTTOM_RIGHT);
		
		return root;
	}
	
	public void actualiserListePanneauxC() {
		for(int i=0;i<PrincipaleFX.getClientLC(indexClient).taillePF();i++)
		{
			this.listePanneaux.getItems().add(PrincipaleFX.getClientLC(indexClient).getPanneau(i));
		}
	}
	
	public void actualiserListeFactures() {
		for(int i=0;i<PrincipaleFX.getClientLC(indexClient).taillePF();i++)
		{
			this.listeFactures.getItems().add(PrincipaleFX.getClientLC(indexClient).getFacture(i));
		}
	}
	
	public void fermer()
	{
		this.close();
		
	}
}