import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreAjoutPanneauClient extends Stage {
	// les widgets de la fenetre		
		private VBox 				saisieFacture	= new VBox();
		private Label 				lblFacture		= new Label("PARTIE FACTURE");
		private Label 				lblDate			= new Label("choisir la date de location :");
		private DatePicker			bnDate			= new DatePicker();
		private Label 				lblDuree		= new Label("choisir la duree de la location :");
		private Spinner<Integer> 	spinDuree 		= new Spinner<Integer>();
		SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1000, 1);
		private Label				lblPrix			= new Label();
		private Label 				lblErreur		= new Label();
		private final int prix=100;

		
		private BorderPane			root			= new BorderPane();
		private VBox 				choixPanneau	= new VBox();
		private Label 				lblPanneau		= new Label("PARTIE PANNEAU");
		private Label 				lblchoix		= new Label("choisir un Panneau a affecter au Client :");
		public ListView<Panneau> 	listePanneaux	= new ListView<Panneau>();
		private HBox 				zoneBoutons 	= new HBox(20);
		private Button 				bnOK			= new Button("OK");
		private Button 				bnAnnuler		= new Button("Annuler");
		
		
		private int indexClient;
		//private String idPanneau;
				
		// constructeur : initialisation de la fenêtre
		public FenetreAjoutPanneauClient(int i){
			this.indexClient=i;
			this.setTitle("Ajouter un Panneau et sa Facture");
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		// creation du Scene graph
		private Parent creerContenu() {
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);
			this.bnOK.setPrefWidth(100);
			this.bnOK.setDisable(true);
			this.bnAnnuler.setPrefWidth(100);
			this.lblDate.setPrefWidth(180);
			this.bnDate.setPrefWidth(180);
			this.lblDuree.setPrefWidth(180);
			this.spinDuree.setPrefWidth(180);
			this.spinDuree.setValueFactory(valueFactory);
			this.lblPrix.setPrefWidth(180);
			this.lblErreur.setPrefWidth(180);
			this.lblErreur.setTextFill(Color.RED);
			
			this.listePanneaux.setOnMouseClicked(e -> griserBoutons());
			this.bnDate.setOnAction(e -> griserBoutons());
			this.bnAnnuler.setOnAction(e-> this.fermer());
			this.bnOK.setOnAction(e->ajoutok());
			this.spinDuree.setOnMouseClicked(e -> genererPrix());

			
			zoneBoutons.getChildren().addAll(bnOK, bnAnnuler);
			zoneBoutons.setAlignment(Pos.BASELINE_RIGHT);
			choixPanneau.getChildren().addAll(lblPanneau,lblchoix,listePanneaux);
			VBox.setMargin(lblchoix, new Insets(20, 0, 0, 0));
			saisieFacture.getChildren().addAll(lblFacture, lblDate, bnDate, lblDuree, spinDuree, lblPrix, lblErreur);
			VBox.setMargin(lblDate, new Insets(20, 0, 0, 0));
			VBox.setMargin(lblDuree, new Insets(20, 0, 0, 0));
			VBox.setMargin(lblPrix, new Insets(20, 0, 0, 0));
			BorderPane.setMargin(choixPanneau, new Insets(20,20,0,20));
			BorderPane.setMargin(saisieFacture, new Insets(20));
			BorderPane.setMargin(zoneBoutons, new Insets(20));
			root.setLeft(choixPanneau);
			root.setRight(saisieFacture);
			root.setBottom(zoneBoutons);
			return root;
		}
		
		public void actualiserListePanneaux() {
			for(int i=0;i<PrincipaleFX.tailleLP();i++)
			{
				this.listePanneaux.getItems().add(PrincipaleFX.getPanneauLP(i));
				for(int y=0;y<PrincipaleFX.tailleLC();y++)
				{
					if(PrincipaleFX.getClientLC(y).containsPanneau(PrincipaleFX.getPanneauLP(i)))
					{
						this.listePanneaux.getItems().remove(PrincipaleFX.getPanneauLP(i));
					}
				}
			}
		}
		
		public void initDate()
		{
			bnDate.setValue(LocalDate.now());
		}
		
		public void genererPrix()
		{
			int prix_location;
			prix_location=(this.prix*Integer.parseInt(this.spinDuree.getEditor().getText())+((this.prix*Integer.parseInt(this.spinDuree.getEditor().getText())))+20/100);
			this.lblPrix.setText("PRIX DE LA LOCATION :  "+Integer.toString(prix_location)+" $");
		}
		
		public Date conversionStringDate(String s)
		{
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
						Date d = new Date();
						d=formatter.parse(s);
				return d;
			} catch (ParseException e) {
		      System.out.println("Exception :" + e);
		      return null;
		    }
		}
		
		public void griserBoutons()
		{
			if(this.listePanneaux.getSelectionModel().getSelectedIndex()==-1 || this.listePanneaux.getItems().size()==0 || bnDate.getValue().isAfter(LocalDate.now()))
			{
				this.bnOK.setDisable(true);
			}
			else
			{
				this.bnOK.setDisable(false);
			}
		}
		
		
		public void ajoutok() 
		{
			LocalDate localDate = bnDate.getValue();
			Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
			Date date = Date.from(instant);
			Facture facture = new Facture(date,Integer.parseInt(this.spinDuree.getEditor().getText()));
			int trouve=-1;
			int i=0;
			if(this.listePanneaux.getSelectionModel().getSelectedIndex()!=-1)
			{
				while(trouve==-1 && i<PrincipaleFX.tailleLP())
				{
					if(this.listePanneaux.getSelectionModel().getSelectedItem().equals(PrincipaleFX.getPanneauLP(i)))
					{
						trouve=i;
					}
					i++;
				}
				if(trouve!=-1)
				{
					i=0;
					int panneauDejaAjoute=-1;
					while(panneauDejaAjoute==-1 && i<PrincipaleFX.tailleLC())
					{
						if(PrincipaleFX.getClientLC(i).containsPanneau(PrincipaleFX.getPanneauLP(trouve)))
						{
							panneauDejaAjoute=i;
						}
						i++;
					}
					if(panneauDejaAjoute==-1)
					{
						PrincipaleFX.getClientLC(indexClient).ajouterPanneau(PrincipaleFX.getPanneauLP(trouve), facture);
						PrincipaleFX.getPanneauLP(trouve).ModifierClient(PrincipaleFX.getClientLC(indexClient));
						fermer();
					}
					else
					{
						this.lblErreur.setText("un autre Client poss�de ce Panneau !");
					}
					
				}
				else
				{
					System.out.println("impossible de trouver le Panneau !");
				}
			}
			else
			{
				this.lblErreur.setText("veuillez choisir un Panneau !");
			}
			
		}
		
		
		public void fermer()
		{
			this.close();
		}

}
