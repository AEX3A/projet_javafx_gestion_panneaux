import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreModifierClient extends Stage {
	// les widgets de la fenêtre
		private VBox 			racine				= new VBox();
		private GridPane 		zoneSaisie 			= new GridPane();
		private HBox 			zoneBoutons 		= new HBox(20);
		private Label 			lblClient			= new Label("Nom du client :");
		private TextField		txtNomClient		= new TextField();
		private Label 			lblDomaine			= new Label("Domaine du client :");
		private ComboBox<String> listeDomaines		= new ComboBox<String>();
		private Label 			message				= new Label("(*) saisie obligatoire");
		private Button 			bnOK				= new Button("OK");
		private Button 			bnAnnuler			= new Button("Annuler");
		static private ObservableList<String> lesDomaines = FXCollections.observableArrayList("Publicitaire","Education","Preventif");
		private int indexClient;

				
		// constructeur : initialisation de la fenêtre
		public FenetreModifierClient(int i){			
			indexClient=i;
			this.setTitle("Modifier le clients");
			//this.setResizable(false);
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		// creation du Scene graph
		private Parent creerContenu() {
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);
			this.txtNomClient.setPrefWidth(100);
			this.listeDomaines.setPrefWidth(150);
			this.bnOK.setPrefWidth(100);
			this.bnAnnuler.setPrefWidth(100);
			
			
			this.bnAnnuler.setOnAction(e-> this.fermer());
			this.bnOK.setDisable(true);
			this.bnOK.setOnAction(e->ajoutok());
			this.txtNomClient.setOnKeyReleased(e->{griserBoutons();});
			this.listeDomaines.setOnAction(e->{griserBoutons();});
			
			listeDomaines.setItems(lesDomaines);
						
			zoneSaisie.setHgap(10);
			zoneSaisie.setVgap(20);
			zoneSaisie.add(lblClient,  0,  0);
			zoneSaisie.add(txtNomClient,  1,  0);
			zoneSaisie.add(lblDomaine,  0,  1);
			zoneSaisie.add(listeDomaines,  1,  1);
			zoneSaisie.add(message,  0, 2);
						
			zoneBoutons.getChildren().addAll(bnOK, bnAnnuler);
			zoneBoutons.setAlignment(Pos.CENTER);
			
			racine.getChildren().addAll(zoneSaisie, zoneBoutons);
			VBox.setMargin(zoneSaisie, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

			
			return racine;
		}
		
		public void griserBoutons(){
			if(txtNomClient.getText().isEmpty() || this.listeDomaines.getSelectionModel().getSelectedIndex()==-1){
				bnOK.setDisable(true);
			}
			else{
				bnOK.setDisable(false);
			}
		}
		
		public void initNomClient()
		{
			this.txtNomClient.setText(PrincipaleFX.getClientLC(this.indexClient).getNom());

		}
		
		public void initDomaineClient()
		{
			this.listeDomaines.setValue(PrincipaleFX.getClientLC(this.indexClient).getDomaine());

		}
		
		public void ajoutok(){
			PrincipaleFX.modifierClient(this.indexClient, this.txtNomClient.getText(), this.listeDomaines.getValue());

			this.close();
		}
		
		public void fermer()
		{
			this.close();
		}

}
