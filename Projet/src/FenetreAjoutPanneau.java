import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreAjoutPanneau extends Stage {
	// les widgets de la fenêtre
		private Client			client;
		private VBox 			racine				= new VBox();
		private GridPane 		zoneSaisie 			= new GridPane();
		private HBox 			zoneBoutons 		= new HBox(20);
		private Label 			lblID				= new Label("ID du panneau :");
		private TextField		id					= new TextField();
		private Label 			lblx				= new Label("coordonee x :");
		private TextField		x					= new TextField();
		private Label 			lbly				= new Label("coordonnee y :");
		private TextField		y					= new TextField();
		private Label 			lblmessage			= new Label("(*) saisie obligatoire");
		private Label 			lblerreurID			= new Label("");
		private Label 			lblerreurXY			= new Label("");
		private Button 			bnOK				= new Button("OK");
		private Button 			bnAnnuler			= new Button("Annuler");
				
		// constructeur : initialisation de la fenêtre
		public FenetreAjoutPanneau(){
			this.setTitle("Ajouter un Panneau");
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		public FenetreAjoutPanneau(double x, double y){
			this.setTitle("Ajouter un Panneau");
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		// création du Scene graph
		private Parent creerContenu() {
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);
			this.bnOK.setPrefWidth(100);
			this.bnAnnuler.setPrefWidth(100);
			this.lblerreurID.setPrefWidth(90);
			this.lblerreurXY.setPrefWidth(150);
			this.lblerreurXY.setPrefHeight(50);
			this.bnOK.setDisable(true);
			
			this.bnAnnuler.setOnAction(e-> this.fermer());
			this.bnOK.setOnAction(e->ajoutok());
			this.id.setOnKeyReleased(e-> {griserBoutons();testErrID();});
			this.x.setOnKeyReleased(e-> {griserBoutons();testErrXY();});
			this.y.setOnKeyReleased(e-> {griserBoutons();testErrXY();});
			
			zoneSaisie.setHgap(10);
			zoneSaisie.setVgap(20);
			
			zoneSaisie.add(lblID,  0,  0);
			zoneSaisie.add(id,  1,  0);
			zoneSaisie.add(lblx,  0,  1);
			zoneSaisie.add(x,  1,  1);
			zoneSaisie.add(lbly,  0,  2);
			zoneSaisie.add(y,  1,  2);
			zoneSaisie.add(lblmessage,  0, 3);
			zoneSaisie.add(lblerreurID,  0, 4);
			zoneSaisie.add(lblerreurXY,  1, 4);
						
			zoneBoutons.getChildren().addAll(bnOK, bnAnnuler);
			zoneBoutons.setAlignment(Pos.CENTER);
			
			racine.getChildren().addAll(zoneSaisie, zoneBoutons);
			VBox.setMargin(zoneSaisie, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));

			
			return racine;
		}

		
		public void griserBoutons(){
			if(x.getText().isEmpty() || y.getText().isEmpty() || id.getText().isEmpty() || !this.lblerreurID.getText().isEmpty() || !this.lblerreurXY.getText().isEmpty())
			{
				bnOK.setDisable(true);
			}
			else{
				bnOK.setDisable(false);
			}
		}
		
		public void initID()
		{
			this.id.setText("");
		}
		
		public void initID(String s)
		{
			this.id.setText(s);
		}
		
		public void initX()
		{
			this.x.setText("");
		}
		
		public void initX(String s)
		{
			this.x.setText(s);
		}
		
		public void initY()
		{
			this.y.setText("");
		}
		
		public void initY(String s)
		{
			this.y.setText(s);
		}
		
		public void testErrID()
		{
			if(!this.id.getText().isEmpty())
			{
				int erreurID=-1;
				for(int i=0;i<PrincipaleFX.tailleLP();i++)
				{
					if(this.id.getText().equals(PrincipaleFX.getPanneauLP(i).getId()))
					{
						erreurID=i;
					}
				}
				if(erreurID!=-1)
				{
					this.lblerreurID.setText("ID d�j� utilis� !");
					this.lblerreurID.setTextFill(Color.RED);
					griserBoutons();
				}
				else
				{
					this.lblerreurID.setText("");
					griserBoutons();
				}
			}
		}
		
		public void testErrXY()
		{
			if(!this.x.getText().isEmpty() && !this.y.getText().isEmpty())
			{
				int XYutilis�s=-1;
				int XYhorsLimite=-1;
				for(int i=0;i<PrincipaleFX.tailleLP();i++)
				{
					if(Double.parseDouble(this.x.getText())==PrincipaleFX.getPanneauLP(i).getX() && Double.parseDouble(this.y.getText())==PrincipaleFX.getPanneauLP(i).getY())
					{
						XYutilis�s=i;
					}
					
					if(Double.parseDouble(this.x.getText())>987 || Double.parseDouble(this.x.getText())<13 || Double.parseDouble(this.y.getText())>595 || Double.parseDouble(this.y.getText())<24)
					{
						XYhorsLimite=i;
					}
				}
				if(XYutilis�s!=-1)
				{
					this.lblerreurXY.setText("coordonn�es d�j� utilis�es !");
					this.lblerreurXY.setTextFill(Color.RED);
					griserBoutons();
				}
				else if(XYhorsLimite!=-1)
				{
					this.lblerreurXY.setText("coordonn�es invalides !\n(12<x<988 ou 23<y<596)");
					this.lblerreurXY.setTextFill(Color.RED);
					griserBoutons();
				}
				else
				{
					this.lblerreurXY.setText("");
					griserBoutons();
				}
			}	
		}
		
		public void ajoutok(){
			try {
				Panneau p = new Panneau(this.id.getText(),Double.parseDouble(this.x.getText()),Double.parseDouble(this.y.getText()));
				PrincipaleFX.ajouterPanneau(p);		
				this.close();
			}
			catch (java.lang.NumberFormatException e){
				System.out.println("format incorrect !");
			}
		}

		public void fermer()
		{
			this.close();
		}

}
