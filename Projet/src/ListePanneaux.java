import java.io.*;
import java.util.ArrayList;

public class ListePanneaux implements Serializable, Fichier {
	private ArrayList<Panneau> tabPanneaux;
	
	public ListePanneaux(){
        this.tabPanneaux = new ArrayList<Panneau>(50);
    }

	void ajouterPanneau(Panneau p)
	{
		if(p!=null && !this.tabPanneaux.contains(p))
		{
			int trouve=-1;
			for(int i=0;i<this.taille();i++)
			{
				if(p.getId().equals(tabPanneaux.get(i).getId()))
				{
					trouve=i;
				}
				
				if(p.getX()==tabPanneaux.get(i).getX() && p.getY()==tabPanneaux.get(i).getY())
				{
					trouve=i;
				}
			}
			if(trouve==-1)
			{
				this.tabPanneaux.add(p);
			}
			else
			{
				System.out.println("ajout impossible");
			}
		}
		else
		{
			System.out.println("ajout impossible");
		}
	}
	
	void supprPanneau(Panneau p)
	{
		if(p!=null && this.tabPanneaux.contains(p))
		{
			this.tabPanneaux.remove(p);
		}
		else
		{
			System.out.println("suppression impossible");
		}
	}
	
	Panneau getPanneau(int i)
	{
		return this.tabPanneaux.get(i);
	}
	
	boolean ListeContains(Panneau p)
	{
		if(this.tabPanneaux.contains(p))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	void listerPanneaux()
	{
		System.out.println("\nliste Panneaux :\n");
		for(int i=0;i<this.tabPanneaux.size();i++)
		{
			System.out.println(this.getPanneau(i));
		}
	}
	
	int taille()
	{
		return this.tabPanneaux.size();
	}
	
	/* PARTIE FICHIERS */
	
	public void charger(){
	    ObjectInputStream ois;
	    try {
	    	ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File("liste_Panneaux.txt"))));
	    	try {	 
		        ArrayList<Panneau> arrayList = (ArrayList<Panneau>)ois.readObject();
		        this.tabPanneaux=(ArrayList<Panneau>)arrayList.clone();
				System.out.println("chargement des donn�es r�ussi !");
	    	}
	        
	        catch (ClassNotFoundException e) 
	    	{
	        	e.printStackTrace();
	        }
		
	    	ois.close();
	    
	    } 
	    	catch (FileNotFoundException e) 
	    {
	    	e.printStackTrace();
	    } 
	    	catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
}
	     
    public void sauvegarder(){
	    ObjectOutputStream oos;
	    try {
	    	oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                new FileOutputStream(
		                  new File("liste_Panneaux.txt"))));
	    	
	    	oos.writeObject(this.tabPanneaux);
	        
	    	oos.close();
	    
	    } 
	    	catch (FileNotFoundException e) 
	    {
	    	e.printStackTrace();
	    } 
	    	catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
    }
}	

