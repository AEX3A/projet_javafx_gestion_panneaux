import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreListeClients extends Stage {
	// les composants de la fenêtre
		
	private AnchorPane  		racine			= new AnchorPane();
	public ListView<String> 	listeClients	= new ListView<String>();
	private Button 				bnAjouter 		= new Button("Ajouter...");
	private Button 				bnAfficher 		= new Button("Afficher...");
	private Button 				bnSupprimer 	= new Button("Supprimer");
	private Button 				bnFermer 		= new Button("Fermer");
	private Button 				bnModifier 		= new Button("Modifier...");
	private Button              bnAjoutPF       = new Button("Ajout Panneau...");
	private Button              bnSupprPF       = new Button("Suppr Panneau...");


	// constructeur : initialisation de la fenêtre
	public FenetreListeClients(){
		this.setTitle("Liste des Clients");
		this.setMinHeight(350);
		this.setMinWidth(350);
		this.setResizable(true);
		this.setScene(new Scene(creerContenu(),350,350));	
	}
	
	// création du Scene graph
	private Parent creerContenu() {
		bnAjouter.setPrefWidth(130);
		bnAfficher.setPrefWidth(130);
		bnModifier.setPrefWidth(130);
		bnAjoutPF.setPrefWidth(130);
		bnSupprPF.setPrefWidth(130);
		bnSupprimer.setPrefWidth(130);
		bnModifier.setDisable(true);
		bnAjoutPF.setDisable(true);
		bnSupprPF.setDisable(true);
		bnSupprimer.setDisable(true);		
		bnAfficher.setDisable(true);
		bnFermer.setPrefWidth(130);
		bnFermer.setPrefHeight(50);
		
		listeClients.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		listeClients.setOnMouseClicked(e->{griserBoutons();});
		this.listeClients.setOnMouseClicked(e -> {griserBoutons();
		doubleClick(e);});
		
		AnchorPane.setTopAnchor(bnAjouter, 10.0);
		AnchorPane.setRightAnchor(bnAjouter, 10.0);
		bnAjouter.setOnAction(e->PrincipaleFX.ouvrirFenetreAjoutClient());
		
		AnchorPane.setTopAnchor(bnAfficher, 80.0);
		AnchorPane.setRightAnchor(bnAfficher, 10.0);
		bnAfficher.setOnAction(e->griserBoutons());
		bnAfficher.setOnAction(e->PrincipaleFX.ouvrirFenetreInfosClient(this.listeClients.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnModifier, 120.0);
		AnchorPane.setRightAnchor(bnModifier, 10.0);
		bnModifier.setOnAction(e->griserBoutons());
		bnModifier.setOnAction(e->PrincipaleFX.ouvrirFenetreModifierClient(this.listeClients.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnAjoutPF, 160.0);
		AnchorPane.setRightAnchor(bnAjoutPF, 10.0);
		bnAjoutPF.setOnAction(e->griserBoutons());
		bnAjoutPF.setOnAction(e->PrincipaleFX.ouvrirFenetreAjoutPanneauClient(this.listeClients.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnSupprPF, 200.0);
		AnchorPane.setRightAnchor(bnSupprPF, 10.0);
		bnSupprPF.setOnAction(e->griserBoutons());
		bnSupprPF.setOnAction(e->PrincipaleFX.ouvirFenetreSuppressionPanneauClient(this.listeClients.getSelectionModel().getSelectedIndex()));
		
		AnchorPane.setTopAnchor(bnSupprimer, 240.0);
		AnchorPane.setRightAnchor(bnSupprimer, 10.0);
		bnSupprimer.setOnAction(e->griserBoutons());
		bnSupprimer.setOnAction(e ->suppression());

		
		AnchorPane.setBottomAnchor(bnFermer, 10.0);
		AnchorPane.setRightAnchor(bnFermer, 10.0);
		//bnModifier.setOnAction(e->{Principale.ouvrirFenetreModifier(listeEtudiants.setOnKeyReleased(), listeEtudiants.getSelectionModel().getSelectedIndex());
		bnFermer.setOnAction(e->this.fermer());
		
		AnchorPane.setTopAnchor(listeClients, 10.0);
		AnchorPane.setLeftAnchor(listeClients, 10.0);
		AnchorPane.setRightAnchor(listeClients, 150.0);
		AnchorPane.setBottomAnchor(listeClients, 10.0);
		

		racine.getChildren().addAll(bnAjouter, bnAfficher, bnModifier, bnSupprimer, bnFermer, bnAjoutPF, bnSupprPF, listeClients);		

		return racine;
	}
	
	public void actualiserListeClients() {
		for(int i=0;i<PrincipaleFX.tailleLC();i++)
		{
			listeClients.getItems().add(i,PrincipaleFX.getClientLC(i).getNom());
		}
	}
	
	public void clearListeClients()
	{
		listeClients.getItems().clear();
	}
	
	public void initBoutons()
	{
		this.bnAfficher.setDisable(true);
		this.bnModifier.setDisable(true);
		this.bnAjoutPF.setDisable(true);
		this.bnSupprPF.setDisable(true);
		this.bnSupprimer.setDisable(true);
	}
	
	public void griserBoutons(){
		if(listeClients.getSelectionModel().getSelectedIndex()!=-1 && listeClients.getItems().size()>0)
		{
			bnSupprimer.setDisable(false);
			bnModifier.setDisable(false);
			bnAjoutPF.setDisable(false);
			bnSupprPF.setDisable(false);
			bnAfficher.setDisable(false);
		}
		else 
		{
			bnSupprimer.setDisable(true);
			bnModifier.setDisable(true);
			bnAjoutPF.setDisable(true);		
			bnSupprPF.setDisable(true);
			bnAfficher.setDisable(true);
		}
	}
	
	public void doubleClick(MouseEvent e)
	{
		if (e.getClickCount()==2){
			PrincipaleFX.ouvrirFenetreInfosClient(this.listeClients.getSelectionModel().getSelectedIndex());
		}
	}
	
	public void suppression(){
		Alert alert=new Alert(AlertType.CONFIRMATION,"Voulez-vous vraiment le supprimer",ButtonType.NO,ButtonType.YES);
		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get()==ButtonType.YES){
			
			int trouve=-1;
			int i=0;
			while(trouve==-1 && i<PrincipaleFX.tailleLC())
			{
				if(PrincipaleFX.getClientLC(i).getNom().equals(this.listeClients.getSelectionModel().getSelectedItem()))
				{
					trouve=i;
				}
				i++;
			}
			if(trouve>=0)
			{
				PrincipaleFX.supprimerClient(PrincipaleFX.getClientLC(trouve));
			}
			else
			{
				System.out.println("erreur de suppression");
			}
			
			//PrincipaleFX.supprimerClient(this.listeClients.getSelectionModel().getSelectedIndex());
		}
	}
	
	public void fermer()
	{
		this.close();
	}
}