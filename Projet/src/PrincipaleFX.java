import java.util.Date;
import java.util.Optional;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PrincipaleFX extends Application { 
	static private FenetreListeClients flisteClients = new FenetreListeClients();
	static private FenetreAjoutClient fajoutClient = new FenetreAjoutClient();
	static private FenetreModifierClient fModifClient;
	static private FenetreAjoutPanneauClient fajoutPanneauClient;
	static private FenetreSupprimerPanneauClient fsupprPFaClient;
	static private FenetreInfosClient fInfosClient;
	static private FenetreListePanneaux flistePanneaux = new FenetreListePanneaux();
	static private FenetreAjoutPanneau fajoutPanneaux = new FenetreAjoutPanneau();
	static private FenetreDeplacerPanneau fdeplacerPanneau;
	static private FenetreMessagePanneau fmessagePanneau;
	static private FenetreInfosPanneau fInfosPanneaux;
	static private FenetrePrincipale fprincipale = new FenetrePrincipale();
	
	
	static private ListeClients	lc = new ListeClients();
	static private ListePanneaux lp = new ListePanneaux();
	static private ObservableList<Panneau> lesPanneauxC = FXCollections.observableArrayList();
	static private ObservableList<Facture> lesFactures = FXCollections.observableArrayList();
	
	public void start(Stage primaryStage) throws Exception {		
		this.ouvrirFenetrePrincipale();
	} 

	
	/////////////////////////////////////////////////////
	// Gestion des fenetres
	/////////////////////////////////////////////////////
	
	
	static public void ouvrirFenetreListeClient(){
		flisteClients.clearListeClients();
		flisteClients.actualiserListeClients();	
		flisteClients.initBoutons();	
		flisteClients.show();
	}
	
	static public void ouvrirFenetreAjoutClient(){
		fajoutClient.initNomClient();
		fajoutClient.show();
	}
	
	static public void ouvrirFenetreModifierClient(int i)
	{	
		fModifClient = new FenetreModifierClient(i);
		fModifClient.initDomaineClient();
		fModifClient.initNomClient();
		fModifClient.show();
	}
	
	static public void ouvrirFenetreInfosClient(int i)
	{	
		fInfosClient = new FenetreInfosClient(i);
		fInfosClient.actualiserListePanneauxC();
		fInfosClient.actualiserListeFactures();
		fInfosClient.show();
	}
	
	static public void ouvrirFenetreAjoutPanneauClient(int i)
	{		
		fajoutPanneauClient = new FenetreAjoutPanneauClient(i);
		fajoutPanneauClient.genererPrix();
		fajoutPanneauClient.initDate();
		fajoutPanneauClient.actualiserListePanneaux();
		fajoutPanneauClient.show();
	}
	
	static public void ouvirFenetreSuppressionPanneauClient(int i)
	{
		fsupprPFaClient = new FenetreSupprimerPanneauClient(i);
		fsupprPFaClient.actualiserListePanneauxC();
		fsupprPFaClient.show();
	}
	
	static public void ouvrirFenetreListePanneaux(){
		flistePanneaux.clearListePanneaux();
		flistePanneaux.actualiserListePanneaux();	
		flistePanneaux.initBoutons();
		flistePanneaux.show();
	}
	
	static public void ouvrirFenetreAjoutPanneau()
	{
		fajoutPanneaux.initID();
		fajoutPanneaux.initX();
		fajoutPanneaux.initY();
		fajoutPanneaux.show();
	}
	
	static public void ouvrirFenetreAjoutPanneau(double x, double y)
	{
		fajoutPanneaux = new FenetreAjoutPanneau(x,y);
		fajoutPanneaux.initX(Double.toString(x));
		fajoutPanneaux.initY(Double.toString(y));
		fajoutPanneaux.show();
	}
	
	static public void ouvrirFenetreInfosPanneau(int i){
		fInfosPanneaux = new FenetreInfosPanneau(i);
		fInfosPanneaux.show();
	}
	
	static public void ouvrirFenetreInfosPanneau(Panneau p){
		fInfosPanneaux = new FenetreInfosPanneau(p);
		fInfosPanneaux.show();
	}
	
	static public void ouvrirFenetreDeplacerPanneau(int i)
	{	
		fdeplacerPanneau = new FenetreDeplacerPanneau(i);
		fdeplacerPanneau.initX();
		fdeplacerPanneau.initY();
		fdeplacerPanneau.show();
	}
	
	static public void ouvrirFenetreMessagePanneau(int i)
	{
		fmessagePanneau = new FenetreMessagePanneau(i);
		fmessagePanneau.initMessage();
		fmessagePanneau.show();
	}
	
	static public void ouvrirFenetrePrincipale()
	{
		fprincipale.show();
		Alert alert=new Alert(AlertType.WARNING,"Voulez-vous charger les données sauvegardées prédécement ?",ButtonType.NO,ButtonType.YES);
		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get()==ButtonType.YES){
			charger();
		}
		actualiserAffichagePanneaux();
	}
	
	/////////////////////////////////////////////////////
	// Gestion de la liste des donnees
	/////////////////////////////////////////////////////
	
	
	public static void ajouterClient(Client c){
		lc.ajouterClient(c);
		lc.listerClients();
		flisteClients.clearListeClients();
		flisteClients.actualiserListeClients();
	}
	
	public static void ajouterPanneauAClient(int indexClient, int indexPanneau, Facture f)
	{
		
	}
	
	public static void modifierClientAPanneau(int indexClient, int indexPanneau, Facture f)
	{
		
	}
	
	public static void modifierClient(int i, String nom, String domaine)
	{
		lc.getClient(i).setNom(nom);
		lc.getClient(i).setDomaine(domaine);
		flisteClients.clearListeClients();
		flisteClients.actualiserListeClients();
	}
	
	public static void supprimerClient(Client c) {
		lc.supprClient(c);
		lc.listerClients();
		flisteClients.clearListeClients();
		flisteClients.actualiserListeClients();
		flisteClients.griserBoutons();
	}
	
	public static Client getClientLC(int i) {
		return lc.getClient(i);
	}
	
	public static int tailleLC()
	{
		return lc.taille();
	}
	
	public static boolean LCcontains(Client c)
	{
		if(lc.ListeContains(c))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean ClientcontainsPanneau(Client c, Panneau p)
	{
		if(c.containsPanneau(p))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void listerLC()
	{
		lc.listerClients();
	}
	
	public static void ajouterPanneau(Panneau p){
		lp.ajouterPanneau(p);
		lp.listerPanneaux();
		flistePanneaux.clearListePanneaux();
		flistePanneaux.actualiserListePanneaux();
		fprincipale.clearCarte();
		actualiserAffichagePanneaux();
		}
	
	public static void deplacerPanneau(int i, double x, double y)
	{
		lp.getPanneau(i).setX(x);
		lp.getPanneau(i).setY(y);
		lp.listerPanneaux();
		fprincipale.clearCarte();
		actualiserAffichagePanneaux();
	}
	
	public static void dragPanneau(int i, double x, double y)
	{
		lp.getPanneau(i).setX(x);
		lp.getPanneau(i).setY(y);
		lp.listerPanneaux();
	}
	
	public static void modifierMessagePanneau(int i, String message)
	{
		lp.getPanneau(i).ajouterMessage(message);
		System.out.println(lp.getPanneau(i).getMessage());
	}
	
	public static void supprimerPanneau(Panneau p) {
		lp.supprPanneau(p);
		lp.listerPanneaux();
		flistePanneaux.clearListePanneaux();
		flistePanneaux.actualiserListePanneaux();
		flisteClients.griserBoutons();
		fprincipale.clearCarte();
		actualiserAffichagePanneaux();
	}
	
	public static Panneau getPanneauLP(int i) {
		return lp.getPanneau(i);
	}
	
	public static int tailleLP()
	{
		return lp.taille();
	}
	
	public static int tailleLPClient(int i)
	{
		return lc.getClient(i).taillePF();
	}
	
	public static boolean LPcontains(Panneau p)
	{
		if(lp.ListeContains(p))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static void listerLP()
	{
		lp.listerPanneaux();
	}
	
	public static void actualiserAffichagePanneaux()
	{
		for(int i=0;i<lp.taille();i++)
		{
			fprincipale.ajoutPanneauCarte(getPanneauLP(i).getX(),getPanneauLP(i).getY());
		}
	}
	
	public static void charger()
	{
		lp.charger();
		lc.charger();
	}
	
	public static void sauvegarder()
	{
		lp.sauvegarder();
		lc.sauvegarder();
	}
	
	public static void main(String[] args) { 
		Application.launch(args); 
	}
}