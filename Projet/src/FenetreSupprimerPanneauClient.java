import java.util.Date;
import java.util.Optional;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreSupprimerPanneauClient extends Stage {
	// les composants de la fenêtre
		
	private BorderPane 			root		 	= new BorderPane();
	private HBox 				zoneBoutons	 	= new HBox();
	private ListView<Panneau>	listePanneaux	= new ListView<Panneau>();
	private Label				lblInfos		= new Label("choisissez le panneau a retirer du client :\nla facture associ�e sera aussi supprim�e !");
	private Button 				bnSelection		= new Button("Selectionner");
	private Button 				bnFermer 		= new Button("Fermer");
	private int indexClient;
	private Date date;


	// constructeur : initialisation de la fenêtre
	public FenetreSupprimerPanneauClient(int i){
		indexClient=i;
		this.setTitle("Suppression d'un paneau du client");
		this.setMinWidth(200);
		this.setScene(new Scene(creerContenu()));	
		this.sizeToScene();
	}
	
	// création du Scene graph
	private Parent creerContenu() {
		this.listePanneaux.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);
		bnFermer.setPrefWidth(100);
		bnSelection.setPrefWidth(100);
		bnSelection.setDisable(true);
		bnFermer.setOnAction(e -> fermer());
		listePanneaux.setOnMouseClicked(e -> griserBoutons());
		bnSelection.setOnAction(e -> suppression());
		
		zoneBoutons.getChildren().addAll(bnSelection,bnFermer);
		zoneBoutons.setMargin(bnFermer, new Insets(10));
		zoneBoutons.setMargin(bnSelection, new Insets(10));
		zoneBoutons.setAlignment(Pos.BOTTOM_CENTER);

		root.setTop(lblInfos);
		root.setCenter(listePanneaux);
		root.setBottom(zoneBoutons);

		BorderPane.setMargin(lblInfos, new Insets(10,10,0,10));
		BorderPane.setMargin(listePanneaux, new Insets(10,10,0,10));
		
		return root;
	}
	
	public void actualiserListePanneauxC() {
		for(int i=0;i<PrincipaleFX.getClientLC(indexClient).taillePF();i++)
		{
			this.listePanneaux.getItems().add(PrincipaleFX.getClientLC(indexClient).getPanneau(i));
		}
	}
	
	public void griserBoutons(){
		if(this.listePanneaux.getSelectionModel().getSelectedIndex()!=-1 && this.listePanneaux.getItems().size()>0)
		{
			bnSelection.setDisable(false);
		}
		else 
		{
			bnSelection.setDisable(true);
		}
	}
	
	public void suppression()
	{
		/*if(lp.getPanneau(indexPanneau).getClient()!=null && !lp.getPanneau(indexPanneau).getClient().equals(lc.getClient(indexClient)))
		{
			System.out.println("panneau affect� � un autre client");
		}
		else
		{
			lc.getClient(indexClient).ajouterPanneau(lp.getPanneau(indexPanneau), f);
			lp.getPanneau(indexPanneau).ajouterClient(lc.getClient(indexClient));
		}*/
		Alert alert=new Alert(AlertType.CONFIRMATION,"Voulez-vous vraiment le supprimer",ButtonType.NO,ButtonType.YES);
		Optional<ButtonType> result = alert.showAndWait();
		if(result.isPresent() && result.get()==ButtonType.YES)
		{
			PrincipaleFX.getClientLC(indexClient).supprPanneau(this.listePanneaux.getSelectionModel().getSelectedItem(),PrincipaleFX.getClientLC(indexClient).getFacture(this.listePanneaux.getSelectionModel().getSelectedIndex()));
			this.close();
		}
		
	}
	
	public void fermer()
	{
		this.close();
		
	}
}