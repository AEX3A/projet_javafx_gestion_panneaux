import java.io.Serializable;
import java.util.Date;

public class Facture implements Serializable{
	private Date date_location;
	private int duree_location;
	private Double prix_location;
	private final int prix = 100;
	
	Facture(Date dal, int dul)
	{
		this.date_location=dal;
		this.duree_location=dul;
		this.prix_location=0.0;
	}

	public Date getDate_location() {
		return date_location;
	}

	public int getDuree_location() {
		return duree_location;
	}
	
	public Double getPrix_location() {
		return prix_location;
	}

	public int getPrix() {
		return prix;
	}

	public void setDate_location(Date date_location) {
		this.date_location = date_location;
	}

	public void setDuree_location(int duree_location) {
		this.duree_location = duree_location;
	}

	public void setPrix_location(Double prix_location) {
		this.prix_location = prix_location;
	}

	public void calculerPrixLocation()
	{
		Double resultat = (prix * Double.valueOf(this.duree_location))*(Double)1.20;
		prix_location=resultat;
	}
	
	public String toString()
	{
		if(this.prix_location!=0)
		{
			return "date de location : "+this.date_location+"\nduree de la location : "+this.duree_location+" jour(s)"+"\nprix de la location : "+this.prix_location;
		}
		else
		{
			return "date de location : "+this.date_location+"\nduree de la location : "+this.duree_location+" jour(s)";
		}
	}
	
	void afficher()
	{
		System.out.println(this.toString());
	}
	
	
	
	

}
