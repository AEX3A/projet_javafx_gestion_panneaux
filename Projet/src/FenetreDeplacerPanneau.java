import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class FenetreDeplacerPanneau extends Stage {
	// les widgets de la fenêtre
		private VBox 			racine				= new VBox();
		private GridPane 		zoneSaisie 			= new GridPane();
		private HBox 			zoneBoutons 		= new HBox(20);
		private Label 			lblx			= new Label("coordon�e x :");
		private TextField		x			= new TextField();
		private Label 			lbly		= new Label("coordonn�e y :");
		private TextField		y			= new TextField();
		private Label 			message				= new Label("(*) saisie obligatoire");
		private Button 			bnOK				= new Button("OK");
		private Button 			bnAnnuler			= new Button("Annuler");
		private int indexPanneau;
				
		// constructeur : initialisation de la fenêtre
		public FenetreDeplacerPanneau(int i){
			this.indexPanneau=i;
			this.setTitle("Deplacer le panneau");
			this.setScene(new Scene(creerContenu()));
			this.sizeToScene();
		}
		
		private Parent creerContenu() {
			this.initModality(Modality.APPLICATION_MODAL);
			this.setResizable(false);		
			this.bnOK.setPrefWidth(100);
			this.bnAnnuler.setPrefWidth(100);
			
			this.bnAnnuler.setOnAction(e-> this.fermer());
			this.bnOK.setDisable(true);
			this.bnOK.setOnAction(e->ajoutok());
			this.x.setOnMouseEntered(e->griserBoutons());
			this.y.setOnMouseEntered(e->griserBoutons());
			
			
			zoneSaisie.setHgap(10);
			zoneSaisie.setVgap(20);
			
			zoneSaisie.add(lblx,  0,  0);
			zoneSaisie.add(x,  1,  0);
			zoneSaisie.add(lbly,  0,  1);
			zoneSaisie.add(y,  1,  1);
			zoneSaisie.add(message,  0, 2);
						
			zoneBoutons.getChildren().addAll(bnOK, bnAnnuler);
			zoneBoutons.setAlignment(Pos.CENTER_RIGHT);
			
			racine.getChildren().addAll(zoneSaisie, zoneBoutons);
			VBox.setMargin(zoneSaisie, new Insets(20, 20, 20, 20));
			VBox.setMargin(zoneBoutons, new Insets(20, 20, 20, 20));


			
			return racine;
		}

		
		public void griserBoutons(){
			if(x.getText().isEmpty() || y.getText().isEmpty())
			{
				bnOK.setDisable(true);
			}
			else{
				bnOK.setDisable(false);
			}
		}
		
		public void initX()
		{
			this.x.setText(Double.toString(PrincipaleFX.getPanneauLP(this.indexPanneau).getX()));

		}
		
		public void initY()
		{
			this.y.setText(Double.toString(PrincipaleFX.getPanneauLP(this.indexPanneau).getY()));

		}
		
		public void ajoutok(){
			try {
				PrincipaleFX.deplacerPanneau(this.indexPanneau, Double.parseDouble(this.x.getText()), Double.parseDouble(this.y.getText()));
			}
			catch (java.lang.NumberFormatException e){
				System.out.println("format incorrect !");
			}
			
			this.close();
		}

		
		public void fermer()
		{
			this.close();
		}

}
