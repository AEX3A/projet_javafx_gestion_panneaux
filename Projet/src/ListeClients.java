import java.io.*;
import java.util.ArrayList;

public class ListeClients implements Serializable, Fichier {
	private ArrayList<Client> tabClients;
	
	public ListeClients(){
        this.tabClients = new ArrayList<Client>();
    }

	void ajouterClient(Client c)
	{
		if(c!=null && !this.tabClients.contains(c))
		{
			this.tabClients.add(c);
		}
		else
		{
			System.out.println("ajout impossible");
		}
	}
	
	void supprClient(Client c)
	{
		if(c!=null && this.tabClients.contains(c))
		{
			this.tabClients.remove(c);
		}
		else
		{
			System.out.println("suppression impossible");
		}
	}
	
	public Client getClient(int i)
	{
		return this.tabClients.get(i);
	}
	
	boolean ListeContains(Client c)
	{
		if(this.tabClients.contains(c))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	void listerClients()
	{
		System.out.println("\nliste Clients :\n");
		for(int i=0;i<this.tabClients.size();i++)
		{
			System.out.println(this.tabClients.get(i));
		}
	}
	
	int taille()
	{
		return this.tabClients.size();
	}
	
	/* PARTIE FICHIERS */
	
	public void charger(){
	    ObjectInputStream ois;
	    try {
	    	ois = new ObjectInputStream(
		              new BufferedInputStream(
		                new FileInputStream(
		                  new File("liste_Clients.txt"))));
	    	try {	  
		        ArrayList<Client> arrayList = (ArrayList<Client>)ois.readObject();
				this.tabClients = arrayList;
				System.out.println("chargement des donn�es r�ussi !");
	    	}
	        
	        catch (ClassNotFoundException e) 
	    	{
	        	e.printStackTrace();
	        }
		
	    	ois.close();
	    
	    } 
	    	catch (FileNotFoundException e) 
	    {
	    	e.printStackTrace();
	    } 
	    	catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
}
	     
    public void sauvegarder(){
	    ObjectOutputStream oos;
	    try {
	    	oos = new ObjectOutputStream(
		              new BufferedOutputStream(
		                new FileOutputStream(
		                  new File("liste_Clients.txt"))));
	    	
	    	oos.writeObject(tabClients);
	        
	    	oos.close();
	    
	    } 
	    	catch (FileNotFoundException e) 
	    {
	    	e.printStackTrace();
	    } 
	    	catch (IOException e) 
	    {
	    	e.printStackTrace();
	    }
    }
}	

